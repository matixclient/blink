package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.game.SendPacketEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;

import net.minecraft.client.entity.EntityOtherPlayerMP;
import net.minecraft.network.Packet;
import net.minecraft.network.play.client.CPacketPlayer;
import net.minecraft.network.play.client.CPacketPlayerBlockPlacement;
import net.minecraft.network.play.client.CPacketPlayerDigging;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

public class ModuleBlink extends Module {
  private Random random;
  private Queue<Packet> packetQueue;
  private int entityID;

  public ModuleBlink() {
    super("Blink", ModuleCategory.MOVEMENT);


    this.setVersion("1.0");
    this.setBuildVersion(15801);
    this.packetQueue = new LinkedList<>();
    this.random = new Random();
  }

  @Override
  public void onEnable() {
    Wrapper.getEventManager().register(this);
    this.entityID = -random.nextInt(99);

    EntityOtherPlayerMP decoyPlayer = new EntityOtherPlayerMP(Wrapper.getWorld(), Wrapper.getPlayer().getGameProfile());
    decoyPlayer.clonePlayer(Wrapper.getPlayer(), true);
    decoyPlayer.rotationYawHead = Wrapper.getPlayer().rotationYawHead;
    decoyPlayer.copyLocationAndAnglesFrom(Wrapper.getPlayer());
    Wrapper.getWorld().addEntityToWorld(this.entityID, decoyPlayer);
  }

  @EventHandler
  public void onSendPacket(SendPacketEvent event) {
    if (event.getPacket() instanceof CPacketPlayerBlockPlacement || event.getPacket() instanceof CPacketPlayerDigging || event.getPacket() instanceof CPacketPlayer) {
      this.packetQueue.add(event.getPacket());

      event.setCancelled(true);
    }
  }

  @Override
  public void onDisable() {
    new Thread(() -> {
      while (!packetQueue.isEmpty()) {
        int rand = 5 + random.nextInt(10);

        for (int i = 0; i < rand; i++) {
          if (!packetQueue.isEmpty()) {
            Packet delayedPacket = packetQueue.remove();

            if (Wrapper.getPlayer() != null) {
              Wrapper.getSendQueue().addToSendQueue(delayedPacket);
            }
          }
        }

        try {
          Thread.sleep(350L);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }).start();

    Wrapper.getWorld().removeEntityFromWorld(this.entityID);

    Wrapper.getEventManager().unregister(this);
  }
}
